/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

//https://kulshekhar.github.io/ts-jest/docs/getting-started/installation
export default {
  globals: {
    "ts-jest": {
      useESM: true,
    },
  },
  moduleNameMapper: {
    // Setting a name mapper allows Jest tests written in TypeScript to know about $lib and $app.
    "^\\$lib(.*)$": "<rootDir>/src/lib$1",
    "^\\$app(.*)$": [
      "<rootDir>/.svelte-kit/dev/runtime/app$1",
      "<rootDir>/.svelte-kit/build/runtime/app$1",
    ],
  },
  //https://kulshekhar.github.io/ts-jest/docs/guides/esm-support/
  preset: "ts-jest/presets/default-esm",
  testEnvironment: "node",
}
