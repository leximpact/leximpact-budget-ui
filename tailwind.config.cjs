const colors = require("tailwindcss/colors")

const config = {
  plugins: [require("@tailwindcss/forms")],
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    screens: {
      sm: "640px",
      // => @media (min-width: 640px) { ... }
      md: "768px",
      // => @media (min-width: 768px) { ... }
      lg: "1024px",
      // => @media (min-width: 1024px) { ... }
      xl: "1280px",
      // => @media (min-width: 1280px) { ... }
      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
      "3xl": "2200px",
      // => @media (min-width: 2200px) { ... }
    },
    extend: {
      colors: {
        gray: colors.neutral,
        current: "currentColor", // here for SVG
        "le-bleu": "#343bff",
        "le-bleu-light": "#d2dfff",
        "le-jaune": "#ded500",
        "le-jaune-dark": "#a6a00c",
        "le-rouge-bill": "#ff6b6b",
        "le-gris-dispositif": "#5E709E",
        "le-gris-dispositif-ultralight": "#EBEFFA",
        "le-gris-dispositif-light": "#CCD3E7",
        "le-gris-dispositif-dark": "#2F406A",
        "le-vert-validation": "#13CC03",
        "le-vert-validation-dark": "#377330",
      },
      blur: {
        xs: "1.2px",
        xxs: "0.8px",
      },
    },
    fontFamily: {
      sans: ["Lato", "sans-serif"],
      serif: ["Lora", "serif"],
    },
  },
  plugins: [
    function ({ addUtilities }) {
      const extendLineThrough = {
        ".line-through-amendment": {
          textDecoration: "line-through",
          "text-decoration-color": "rgba(222, 213, 0,0.7)",
          "text-decoration-thickness": "2px",
        },
        ".line-through-bill": {
          textDecoration: "line-through",
          "text-decoration-color": "rgba(255, 107, 107,0.7)",
          "text-decoration-thickness": "2px",
        },
      }
      addUtilities(extendLineThrough)
    },
  ],
}

module.exports = config
