export type {
  Decomposition as DecompositionCore,
  DecompositionByName as DecompositionCoreByName,
} from "@openfisca/json-model"
import type {
  Decomposition as DecompositionCore,
  DecompositionByName as DecompositionCoreByName,
  DecompositionReference,
  Variable,
} from "@openfisca/json-model"

import decompositionCoreByNameUnknown from "@openfisca/france-json/decompositions.json"

export interface Decomposition extends DecompositionCore {
  name: string
  open?: boolean
}

export type DecompositionByName = { [name: string]: Decomposition }

export const decompositionCoreByName: DecompositionCoreByName =
  decompositionCoreByNameUnknown

export const decompositionsOptionsVariablesName = new Set<string>()
for (const decompositionCore of Object.values(decompositionCoreByName)) {
  if (decompositionCore.options === undefined) {
    continue
  }
  for (const options of decompositionCore.options) {
    if (options.waterfall !== undefined) {
      continue
    }
    for (const variableName of Object.keys(options)) {
      if (["else", "then"].includes(variableName)) {
        continue
      }
      decompositionsOptionsVariablesName.add(variableName)
    }
  }
}

export function buildDecompositionByNameFromCore(
  decompositionCoreByName: DecompositionCoreByName,
): DecompositionByName | undefined {
  return Object.fromEntries(
    Object.entries(decompositionCoreByName).map(([name, decompositionCore]) => [
      name,
      {
        ...decompositionCore,
        name,
      },
    ]),
  )
}

export function* walkDecompositionsCore(
  decompositionCoreByName: DecompositionCoreByName,
  waterfallName: string,
  name: string,
  depthFirst: boolean,
): Generator<[string, DecompositionCore], void, unknown> {
  const decompositionCore = decompositionCoreByName[name]
  if (decompositionCore === undefined) {
    return
  }
  if (!depthFirst) {
    yield [name, decompositionCore]
  }

  let children = decompositionCore.children
  for (const options of decompositionCore.options ?? []) {
    if (options.waterfall !== undefined) {
      if (
        (options as WaterfallOptions).then?.children !== undefined &&
        (options as WaterfallOptions).waterfall.includes(waterfallName)
      ) {
        children = (options as WaterfallOptions).then.children ?? undefined
      } else if (
        (options as WaterfallOptions).else?.children !== undefined &&
        !(options as WaterfallOptions).waterfall.includes(waterfallName)
      ) {
        children = (options as WaterfallOptions).else.children ?? undefined
      }
    }
  }
  if (children !== undefined) {
    for (const childReference of children) {
      yield* walkDecompositionsCore(
        decompositionCoreByName,
        waterfallName,
        childReference.name,
        depthFirst,
      )
    }
  }

  if (depthFirst) {
    yield [name, decompositionCore]
  }
}

export function* walkDecompositionsCoreName(
  decompositionCoreByName: DecompositionCoreByName,
  waterfallName: string,
  name: string,
  depthFirst: boolean,
): Generator<string, void, unknown> {
  for (const [decompositionName] of walkDecompositionsCore(
    decompositionCoreByName,
    waterfallName,
    name,
    depthFirst,
  )) {
    yield decompositionName
  }
}
