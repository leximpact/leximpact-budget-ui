// CONFIGURATION
// -------------

import type { Writable } from "svelte/store"
import type { Reform } from "$lib/reforms"

// TYPES
// -----

type oneToNine = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
type zeroToNine = `${oneToNine}` | 0
type DD = `${0}${oneToNine}` | `${1 | 2}${zeroToNine}` | `3${0 | 1}`
type MM = `0${oneToNine}` | `1${0 | 1 | 2}`
type YYYY = `19${zeroToNine}${zeroToNine}` | `20${zeroToNine}${zeroToNine}`
type DateString = `${YYYY}-${MM}-${DD}`

type ParameterScale = {
  scale: ParameterScaleBracket[]
  start: DateString
  type: "scale"
}

type ParameterScaleBracket = {
  rate: {
    value: number
  }
  threshold: {
    value: number
  }
}

export type AmendementRequest = {
  [openfiscaParameterPath: string]: number | ParameterScale
  // possible : [openfiscaParameterPath in TypeStringOuAutreString: string]: number
}

type ApiRequest = {
  base: number // année de la loi de base sur laquelle on applique le plf et un amendement
  plf?: number
  amendement?: Writable<Reform>
  output_variables: string[] // state_budget + variables des quantiles (X et Y) & entités identiques
  quantile_nb: number // optionnel pour les tests ; si non défini, pas de quantile (0 par défaut)
  quantile_base_variable: string[] // optionnel si pas de quantile demandé (histogram X)
  quantile_compare_variables: string[] // optionnel si pas de quantile demandé (histogram Y)
}

export type ApiResponseQuantile = {
  // un quantile pour base ou plf ou amendement
  fraction: number
  rfr: number // rfr plafond du quantile
  csg?: number
}

export type ApiResponse = {
  result?: {
    base: {
      state_budget: {
        // selon ce qu'on a mis dans 'output_variables' de la requête
        csg?: number
      }
      quantiles?: ApiResponseQuantile[]
    }
    plf?: {
      state_budget: {
        csg?: number
      }
      quantiles?: ApiResponseQuantile[]
    }
    amendement?: {
      state_budget: {
        csg?: number
      }
      quantiles?: ApiResponseQuantile[]
    }
  }
  error?: string
}

// FUNCTIONS
// ---------

export async function calculateBudget(
  fetch,
  apiBudgetStateSimulationUrl: string,
  jwt: string,
  base, // year
  plf, // year
  amendement, // Writable<Reform>
  outputVariables: string[],
  quantileBaseVariable: string[],
  quantileCompareVariables: string[],
): Promise<ApiResponse> {
  let responseJson: ApiResponse | undefined = undefined

  let request: ApiRequest = {
    base: base,
    output_variables: outputVariables,
    quantile_nb: 10,
    quantile_base_variable: quantileBaseVariable,
    quantile_compare_variables: quantileCompareVariables,
  }

  if (amendement === undefined) {
    console.log("Pas d'amendement à calculer.")
  } else {
    console.log("Amendement à calculer :", amendement)
    request.amendement = { ...amendement }
  }

  if (plf === undefined) {
    console.log("Pas de PLF à calculer.")
  } else {
    console.log("PLF à calculer :", plf)
    request.plf = plf
  }

  console.log("Appel à " + apiBudgetStateSimulationUrl + "...")
  const response = await fetch(apiBudgetStateSimulationUrl, {
    //resolved promise
    method: "POST",
    mode: "cors",
    headers: {
      accept: "application/json",
      "Content-Type": "application/json",
      "jwt-token": jwt,
    },
    //credentials: "omit", // <- À décommenter si, côté serveur, nous avons : Allow-Origin: *
    //credentials: "include",
    body: JSON.stringify(request),
  })

  if (response.ok) {
    responseJson = await response.json()
    // console.log("Réponse json (" + outputVariables + "):", responseJson)
    if (responseJson.error) {
      console.error("Erreur API Web : " + responseJson.error)
    }
  } else if (response.status >= 400 && response.status < 500) {
    responseJson = await response.json()
    if (response.status == 401) {
      console.error(
        "Requête refusée. Token invalide (" +
          response.status +
          ": " +
          response.statusText +
          ").",
      )
    } else if (response.status == 429) {
      console.error(
        "Requête refusée. Maximum de requêtes autorisées atteint (" +
          response.status +
          ": " +
          response.statusText +
          ").",
      )
    } else {
      console.error(
        "Erreur client HTTP (" +
          response.status +
          ": " +
          response.statusText +
          ").",
      )
    }
  } else if (response.status == 503) {
    //reddis - cache for connections number is down :'-( - restart server?
    console.error(
      "Une erreur serveur est survenue. Demander une intervention de l'équipe technique (" +
        response.status +
        ": " +
        response.statusText +
        ").",
    )
  } else {
    console.error("Erreur inattendue : ", response.status)
  }

  return responseJson
}
