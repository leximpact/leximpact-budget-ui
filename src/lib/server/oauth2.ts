import { SvelteKitAuth } from "sk-auth"
import { OAuth2Provider } from "sk-auth/providers"

import config from "$lib/server/config"
import type { User } from "$lib/users"

type Profile = User

interface Tokens {
  access_token: string
  expires_in: number
  id_token: string
  "not-before-policy"?: number
  refresh_expires_in?: number
  refresh_token?: string
  session_state?: string
  scope: string // "openid email profile"
  token_type: string // "Bearer"
}

const { baseUrl, oauth2 } = config
const baseUrlItems = new URL(baseUrl)

export const oauth2Authenticator =
  oauth2 === undefined
    ? undefined
    : new SvelteKitAuth({
        basePath: "/authentication",
        // callbacks: {
        //   jwt(token, profile) {
        //     if (profile !== undefined) {
        //       // const { provider, ...account } = profile
        //       token = {
        //         ...token,
        //         user: {
        //           ...(token.user ?? {}),
        //           ...profile,
        //         },
        //         // user: {
        //         //   ...(token.user ?? {}),
        //         //   connections: {
        //         //     ...(token.user?.connections ?? {}),
        //         //     [provider]: account,
        //         //   },
        //         // },
        //       }
        //     }

        //     return token
        //   },
        // },
        host: baseUrlItems.host,
        jwtSecret: oauth2.jwtSecret,
        protocol: baseUrlItems.protocol.replace(/:$/, ""),
        providers: [
          new OAuth2Provider<Profile, Tokens>({
            accessTokenUrl: oauth2.accessTokenUrl,
            // authorizationParams?: any;
            authorizationUrl: oauth2.authorizationUrl,
            clientId: oauth2.clientId,
            clientSecret: oauth2.clientSecret,
            // contentType: "application/json",
            contentType: "application/x-www-form-urlencoded",
            // grantType: "authorization_code",
            // headers?: any;
            id: "leximpact",
            // params: any;
            profileUrl: oauth2.profileUrl,
            // responseType: "code",
            scope: ["openid", "profile", "email"],

            profile(profile) {
              return { ...profile, provider: "leximpact" }
            },
          }),
        ],
      })
