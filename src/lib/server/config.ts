import "dotenv/config"

import { validateConfig } from "$lib/auditors/config"

export interface Config {
  baseUrl: string
  githubPersonalAccessToken?: string
  matomo?: {
    prependDomain?: boolean
    siteId: number
    subdomains?: string
    url: string
  }
  oauth2?: {
    accessTokenUrl: string
    authorizationUrl: string
    clientId: string
    clientSecret: string
    jwtSecret: string
    profileUrl: string
  }
  openfiscaRepository: {
    branch: string
    group: string
    project: string
    rawUrlTemplate: string
    urlTemplate: string
  }
  portalUrl: string
  proxy: boolean
  title: string

  // Budget
  budget: {
    apiBudgetUrl: string
    apiBudgetEndpointName: string
    calculationBaseYear: number
    calculationPlfYear: number
  }
}

const [validConfig, error] = validateConfig({
  baseUrl: process.env["BASE_URL"],
  githubPersonalAccessToken: process.env["GITHUB_PERSONAL_ACCESS_TOKEN"],
  matomo:
    process.env["MATOMO_SITE_ID"] && process.env["MATOMO_URL"]
      ? {
          prependDomain: process.env["MATOMO_PREPEND_DOMAIN"],
          siteId: process.env["MATOMO_SITE_ID"],
          subdomains: process.env["MATOMO_SUBDOMAINS"],
          url: process.env["MATOMO_URL"],
        }
      : null,
  oauth2: process.env["OAUTH2_CLIENT_ID"]
    ? {
        accessTokenUrl: process.env["OAUTH2_ACCESS_TOKEN_URL"],
        authorizationUrl: process.env["OAUTH2_AUTHORIZATION_URL"],
        clientId: process.env["OAUTH2_CLIENT_ID"],
        clientSecret: process.env["OAUTH2_CLIENT_SECRET"],
        jwtSecret: process.env["OAUTH2_JWT_SECRET"],
        profileUrl: process.env["OAUTH2_PROFILE_URL"],
      }
    : null,
  openfiscaRepository: {
    branch: process.env["OPENFISCA_BRANCH"],
    group: process.env["OPENFISCA_GROUP"],
    project: process.env["OPENFISCA_PROJECT"],
    rawUrlTemplate: process.env["OPENFISCA_REPOSITORY_RAW_URL_TEMPLATE"],
    urlTemplate: process.env["OPENFISCA_REPOSITORY_URL_TEMPLATE"],
  },
  portalUrl: process.env["PORTAL_URL"],
  proxy: process.env["PROXY"],
  title: process.env["TITLE"],

  // Budget
  budget: process.env["API_BUDGET_URL"]
    ? {
        apiBudgetUrl: process.env["API_BUDGET_URL"],
        apiBudgetEndpointName: process.env["API_BUDGET_ENDPOINT_NAME"],
        calculationBaseYear: process.env["CALCULATION_BASE_YEAR"],
        calculationPlfYear: process.env["CALCULATION_PLF_YEAR"],
      }
    : null,
})
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
const config = validConfig as Config

export default config
