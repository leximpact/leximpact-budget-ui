import type { Audit } from "@auditors/core"
import {
  auditBoolean,
  auditFunction,
  auditHttpUrl,
  auditInteger,
  auditRequire,
  auditSetNullish,
  auditString,
  auditStringToBoolean,
  auditStringToNumber,
  auditSwitch,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["baseUrl", "portalUrl"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }
  audit.attribute(data, "budget", true, errors, remainingKeys, auditBudget)
  audit.attribute(
    data,
    "title",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditRequire,
  )
  audit.attribute(
    data,
    "githubPersonalAccessToken",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(data, "matomo", true, errors, remainingKeys, auditMatomo)
  audit.attribute(
    data,
    "openfiscaRepository",
    true,
    errors,
    remainingKeys,
    auditOpenFiscaRepository,
    auditRequire,
  )
  audit.attribute(data, "oauth2", true, errors, remainingKeys, auditOauth2)
  for (const key of ["proxy"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
      auditSetNullish(false),
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditMatomo(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "prependDomain",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  audit.attribute(
    data,
    "siteId",
    true,
    errors,
    remainingKeys,
    auditStringToNumber,
    auditInteger,
    auditRequire,
  )
  audit.attribute(
    data,
    "subdomains",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Ensure there is a single trailing "/" in URL.
    auditFunction((url) => url.replace(/\/$/, "") + "/"),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditOauth2(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["accessTokenUrl", "authorizationUrl", "profileUrl"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }
  for (const key of ["clientId", "clientSecret", "jwtSecret"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditOpenFiscaRepository(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["branch", "group", "project"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  for (const key of ["rawUrlTemplate", "urlTemplate"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditBudget(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "apiBudgetUrl",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  audit.attribute(
    data,
    "apiBudgetEndpointName",
    true,
    errors,
    remainingKeys,
    auditString,
  )

  for (const key of ["calculationBaseYear", "calculationPlfYear"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditStringToNumber,
      auditInteger,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
