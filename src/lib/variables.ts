import type {
  Formula,
  Parameter,
  Reference,
  Variable,
  VariableByName,
} from "@openfisca/json-model"

import variableSummaryByNameUnknown from "@openfisca/france-json/variables_summaries.json"
import { getParameter } from "$lib/parameters"

export type VariableValues = boolean[] | number[] | string[]

export interface InstantFormulaAndReferences {
  formula?: Formula
  instant: string
  references: Reference[]
}

export const variableSummaryByName =
  variableSummaryByNameUnknown as VariableByName

export function* iterVariableParameters(
  variable: Variable,
  date: string,
  encounteredParametersName: Set<string> = new Set(),
  encounteredVariablesName: Set<string> = new Set(),
): Generator<Parameter, void> {
  const name = variable.name
  if (encounteredVariablesName.has(name)) {
    return
  }
  encounteredVariablesName.add(name)

  const formulas = variable.formulas
  if (formulas === undefined) {
    return
  }
  const dates = Object.keys(formulas).reverse()
  let formula = undefined
  for (const bestDate of dates) {
    if (bestDate <= date) {
      formula = formulas[bestDate]
      break
    }
  }
  if (formula == null) {
    // No candidate date less than or equal to date found.
    return
  }

  const referredVariablesName = formula.variables
  if (referredVariablesName !== undefined) {
    for (const referredVariableName of referredVariablesName) {
      const referredVariable = variableSummaryByName[referredVariableName]
      yield* iterVariableParameters(
        referredVariable,
        date,
        encounteredParametersName,
        encounteredVariablesName,
      )
    }
  }

  const referredParametersName = formula.parameters
  if (referredParametersName !== undefined) {
    for (const referredParameterName of referredParametersName) {
      if (encounteredParametersName.has(referredParameterName)) {
        continue
      }
      encounteredParametersName.add(referredParameterName)

      const referredParameter = getParameter(referredParameterName)
      if (referredParameter === undefined) {
        continue
      }
      yield referredParameter
    }
  }
}
