const CSG_TITLE =
  "Recettes de CSG des revenus d'activité du secteur privé/public"

const CSG_DESCRIPTION = "Montants estimés pour l'année"

const HISTOGRAM_VARIABLES_TO_SUM = [
  "csg_imposable_salaire",
  "csg_deductible_salaire",
]

const API_OUTPUT_VARIABLES: string[] = [
  "assiette_csg_abattue",
  "assiette_csg_non_abattue",
  "csg_imposable_salaire",

  "csg_deductible_salaire",
]
const API_QUANTILES_BASE_VARIABLES: string[] = [
  "assiette_csg_abattue",
  "assiette_csg_non_abattue",
]

const API_QUANTILES_COMPARE_VARIABLES: string[] = [
  "csg_imposable_salaire",
  "csg_deductible_salaire",
]

export {
  CSG_TITLE,
  CSG_DESCRIPTION,
  HISTOGRAM_VARIABLES_TO_SUM,
  API_OUTPUT_VARIABLES,
  API_QUANTILES_BASE_VARIABLES,
  API_QUANTILES_COMPARE_VARIABLES,
}
