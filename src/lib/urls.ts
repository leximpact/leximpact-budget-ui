export interface SelfTargetAProps {
  href: string
  "sveltekit:prefetch"?: boolean
  "sveltekit:noscroll"?: boolean
}

export function newSelfTargetAProps(url: string): SelfTargetAProps {
  return {
    href: url,
    "sveltekit:prefetch": true,
  }
}
