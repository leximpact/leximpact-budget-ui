import fetch from "node-fetch" // v3 de node-fetch (cf. package.json)
import type { JsonValue } from "@openfisca/json-model"

import { ApiResponse, calculateBudget } from "$lib/components/budget/api-budget"
import { getParameter } from "$lib/parameters"
import {
  API_OUTPUT_VARIABLES,
  API_QUANTILES_BASE_VARIABLES,
  API_QUANTILES_COMPARE_VARIABLES,
} from "$lib/config-csg-activite"
import config from "$lib/server/config"

test("compare simple parameter format in openfisca and reform", () => {
  // reform format where the parameter value is unchanged
  const reform_format = {
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux": {
      start: "2022-01-01",
      type: "parameter",
      value: 0.024,
    },
  }
  const openfisca_format = getParameter(
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux",
  ) as unknown as JsonValue

  // this check only explicits formats differences
  expect(openfisca_format["values"]["1993-07-01"]["value"]).toBe(
    reform_format[
      "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux"
    ]["value"],
  )
})

test("compare scale parameter format in openfisca and reform", () => {
  const reform_format = {
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement":
      {
        scale: [
          {
            rate: {
              value: 0.0175,
            },
            threshold: {
              value: 0,
            },
          },
          {
            rate: {
              value: 0,
            },
            threshold: {
              value: 4,
            },
          },
        ],
        start: "2022-01-01",
        type: "scale",
      },
  }
  const openfisca_format = getParameter(
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement",
  )

  // this check only explicits formats differences
  expect(openfisca_format["brackets"][0]["rate"]["2012-01-01"]["value"]).toBe(
    reform_format[
      "prelevements_sociaux.contributions_sociales.csg.activite.imposable.abattement"
    ]["scale"][0]["rate"]["value"],
  )
})

test("calculateBudget", () => {
  const API_ENDPOINT_URL =
    "https://api-simu-etat-integ.leximpact.dev/state_simulation"
  const YEAR_BASE = "2022"
  const YEAR_PLF = ""
  const UNKNOWN_USER = config.oauth2.jwtSecret
  const AMENDEMENT_EXAMPLE = {
    // AmendementRequest
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux": 0.064,
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.taux": 0.028,
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.abattement":
      {
        scale: [
          { rate: { value: 0.5 }, threshold: { value: 0 } },
          { rate: { value: 0 }, threshold: { value: 4 } },
        ],
        start: "2021-01-01",
        type: "scale",
      },
  }
  const AMENDEMENT_EXAMPLE_2 = {
    // Writable<Reform>
    "prelevements_sociaux.contributions_sociales.csg.activite.deductible.abattement":
      {
        scale: [
          {
            rate: {
              value: 0.03,
            },
            threshold: {
              value: 0,
            },
          },
          {
            rate: {
              value: 0,
            },
            threshold: {
              value: 4,
            },
          },
        ],
        start: "2021-01-01",
        type: "scale",
      },
    "prelevements_sociaux.contributions_sociales.csg.activite.imposable.taux": {
      start: "2021-01-01",
      type: "parameter",
      value: 0.04,
    },
    subscribe: undefined,
    set: undefined,
    update: undefined,
  }

  const RESPONSE_EXAMPLE: ApiResponse = {
    result: {
      base: {
        state_budget: {
          csg: 75_706_000_000,
        },
        quantiles: [
          { fraction: 1, rfr: 0, csg: -1 },
          { fraction: 2, rfr: 0, csg: -2 },
          { fraction: 3, rfr: 0, csg: -3 },
          { fraction: 4, rfr: 0, csg: -4 },
          { fraction: 5, rfr: 0, csg: -5 },
          { fraction: 6, rfr: 0, csg: -6 },
          { fraction: 7, rfr: 0, csg: -7 },
          { fraction: 8, rfr: 0, csg: -8 },
          { fraction: 9, rfr: 0, csg: -9 },
          { fraction: 10, rfr: 0, csg: -10 },
        ],
      },
    },
  }

  calculateBudget(
    fetch,
    API_ENDPOINT_URL,
    UNKNOWN_USER,
    YEAR_BASE,
    YEAR_PLF,
    AMENDEMENT_EXAMPLE_2,
    API_OUTPUT_VARIABLES,
    API_QUANTILES_BASE_VARIABLES,
    API_QUANTILES_COMPARE_VARIABLES,
  )
})
// TODO authentification openid connect for script
