import type {
  Metadata,
  NodeParameter,
  Parameter,
  Reference,
  ScaleAtInstant,
  ScaleParameter,
  ValueAtInstant,
  ValueParameter,
} from "@openfisca/json-model"
import {
  AmountUnit,
  improveParameter,
  ParameterClass,
  RateUnit,
  scaleByInstantFromBrackets,
  ScaleType,
  Unit,
  ValueType,
} from "@openfisca/json-model"

import rootParameterUnknown from "@openfisca/france-json/editable_processed_parameters.json"

export interface InstantReferencesAndScale {
  instant: string
  references: Reference[]
  scaleAtInstant?: ScaleAtInstant
}

export interface InstantReferencesAndValue {
  instant: string
  references: Reference[]
  valueAtInstant?: ValueAtInstant
}

improveParameter(null, rootParameterUnknown as unknown as NodeParameter)
export const rootParameter = rootParameterUnknown as unknown as NodeParameter

export function buildInstantReferencesAndScaleArray(
  parameter: ScaleParameter,
): InstantReferencesAndScale[] {
  const scaleByInstant = scaleByInstantFromBrackets(parameter.brackets ?? [])
  const instantReferencesAndScaleByInstant: {
    [instant: string]: InstantReferencesAndScale
  } = Object.fromEntries(
    Object.entries(scaleByInstant).map(([instant, scaleAtInstant]) => [
      instant,
      { instant, references: [], scaleAtInstant },
    ]),
  )
  if (parameter.reference !== undefined) {
    for (const [instant, references] of Object.entries(parameter.reference)) {
      if (instantReferencesAndScaleByInstant[instant] === undefined) {
        instantReferencesAndScaleByInstant[instant] = {
          instant,
          references,
        }
      } else {
        instantReferencesAndScaleByInstant[instant].references = references
      }
    }
  }
  return Object.entries(instantReferencesAndScaleByInstant)
    .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
    .map(([, instantReferencesAndScale]) => instantReferencesAndScale)
}

export function buildInstantReferencesAndValueArray(
  parameter: ValueParameter,
): InstantReferencesAndValue[] {
  const instantReferencesAndValueByInstant: {
    [instant: string]: InstantReferencesAndValue
  } = Object.fromEntries(
    Object.entries(parameter.values ?? {}).map(([instant, valueAtInstant]) => [
      instant,
      { instant, references: [], valueAtInstant },
    ]),
  )
  if (parameter.reference !== undefined) {
    for (const [instant, references] of Object.entries(parameter.reference)) {
      if (instantReferencesAndValueByInstant[instant] === undefined) {
        instantReferencesAndValueByInstant[instant] = {
          instant,
          references,
        }
      } else {
        instantReferencesAndValueByInstant[instant].references = references
      }
    }
  }
  return Object.entries(instantReferencesAndValueByInstant)
    .sort(([instant1], [instant2]) => instant2.localeCompare(instant1))
    .map(([, instantReferencesAndValue]) => instantReferencesAndValue)
}

export function getParameter(name: string): Parameter | undefined {
  let parameter = rootParameter as Parameter
  for (const id of name.split(".")) {
    const children =
      parameter.class === ParameterClass.Node ? parameter.children : undefined
    if (children === undefined) {
      return undefined
    }
    parameter = children[id]
    if (parameter === undefined) {
      return undefined
    }
  }
  return parameter
}

export function labelFromParameterClass(
  parameterClass: ParameterClass | string,
): string {
  return (
    {
      [ParameterClass.Node]: "Nœud",
      [ParameterClass.Scale]: "Barème",
      [ParameterClass.Value]: "Valeur",
    }[parameterClass] ?? parameterClass
  )
}

export function labelFromScaleType(type: ScaleType | string): string {
  return (
    {
      [ScaleType.LinearAverageRate]: "à taux moyen linéaire",
      [ScaleType.MarginalAmount]: "à montant marginal",
      [ScaleType.MarginalRate]: "à taux marginal",
      [ScaleType.SingleAmount]: "à montant unique",
    }[type] ?? type
  )
}

export function labelFromUnit(
  metadata: Metadata,
  unit: AmountUnit | RateUnit | Unit | string,
): string {
  return (
    {
      [Unit.CodesDepartements]: "code département",
      [Unit.CountryCode]: "code pays",
      [Unit.Currency]: metadata.currency,
      [Unit.CurrencyFRF]: "FRF",
      [Unit.CurrencyGBP]: "£",
      [Unit.CurrencyUSD]: "$",
      [Unit.Day]: "jour",
      [Unit.Month]: "mois",
      [Unit.Rate]: "ratio", // Number between 0 and 1
      [Unit.Year]: "année",
    }[unit] ?? unit
  )
}

export function labelFromValueType(type: ValueType | string): string {
  return (
    {
      [ValueType.Boolean]: "booléen",
      [ValueType.Number]: "nombre",
      [ValueType.StringArray]: "tableau de chaînes de caractères",
      [ValueType.StringByString]: "dictionnaire de chaines de caractères",
    }[type] ?? type
  )
}
