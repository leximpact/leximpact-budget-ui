import type { Metadata } from "@openfisca/json-model"

import metadataUnknown from "@openfisca/france-json/metadata.json"

export const metadata: Metadata = metadataUnknown
