import type { ScaleAtInstant } from "@openfisca/json-model"

export interface Reform {
  [parameterName: string]: ReformChange
}

export type ReformChange = ReformChangeParameter | ReformChangeScale

export interface ReformChangeBase {
  start: string
  stop?: string
  type: ReformChangeType
}

export interface ReformChangeParameter extends ReformChangeBase {
  type: ReformChangeType.Parameter
  value: number
}

export interface ReformChangeScale extends ReformChangeBase {
  type: ReformChangeType.Scale
  scale: ScaleAtInstant
}

export enum ReformChangeType {
  Parameter = "parameter",
  Scale = "scale",
}
