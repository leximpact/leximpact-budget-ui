/// <reference types="@sveltejs/kit" />

declare namespace App {
  //interface Locals {
  //Echanges sur serveur
  //}

  // interface Platform { //Compilation - ici sur node }

  export interface Session {
    //Echange entre serveur et client
    baseUrl: string
    authenticationEnabled: boolean
    hasGithubPersonalAccessToken: boolean
    jwt: string
    matomo?: {
      prependDomain?: boolean
      siteId: number
      subdomains?: string
      url: string
    }
    budget: {
      apiBudgetUrl: string
      apiBudgetEndpointName: string
      calculationBaseYear: number
      calculationPlfYear: number
    }
    openfiscaRepository: SessionOpenFiscaRepository
    portalUrl: string
    title: string
    user?: User
  }

  export interface SessionOpenFiscaRepository {
    branch: string
    group: string
    project: string
    rawUrlTemplate: string
    urlTemplate: string
  }

  // interface Stuff { //Communication d'info entre __layout et pages enfants }

  export interface User {
    //Authentification OpenId Connect
    email: string // "john@doe.com"
    email_verified: boolean
    family_name: string // "Doe"
    given_name: string // "John"
    last_name: string // "Doe"
    locale: string // "fr"
    name: string // "John Doe"
    preferred_username: string // "jdoe",
    roles?: string[] // [ 'offline_access', 'default-roles-leximpact', 'uma_authorization' ],
    sub: string // "12345678-9abc-def0-1234-56789abcdef0"
  }
}
