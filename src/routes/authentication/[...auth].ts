import { oauth2Authenticator } from "$lib/server/oauth2"

export const { get, post } = oauth2Authenticator ?? { get: null, post: null }
