<script context="module" lang="ts">
  import { strictAudit } from "@auditors/core"
  import {
    auditRawParameterToEditable,
    iterParameterAncestors,
    ParameterClass,
  } from "@openfisca/json-model"
  import type { Load } from "@sveltejs/kit"
  import yaml from "js-yaml"

  import { metadata } from "$lib/metadata"
  import { getParameter, labelFromParameterClass } from "$lib/parameters"

  export const load: Load = async function ({ fetch, params, session }) {
    const { parameter: name } = params
    const processedParameter = getParameter(name)
    if (processedParameter === undefined) {
      return {
        status: 404,
        error: new Error(`Parameter "${name}" not found`),
      }
    }

    let unprocessedParameterUrl = newParameterRepositoryRawUrl(
      session.openfiscaRepository,
      processedParameter,
    )
    if (unprocessedParameterUrl === undefined) {
      return {
        status: 400,
        error: new Error(
          `L'édition du paramètre "${processedParameter.name}" est impossible car le paramètre est généré automatiquement et n'a pas de fichier source.`,
        ),
      }
    }
    if (processedParameter.class === ParameterClass.Node) {
      unprocessedParameterUrl += "/index.yaml"
    }
    let unprocessedParameterResponse = await fetch(unprocessedParameterUrl)
    if (!unprocessedParameterResponse.ok) {
      return {
        status: unprocessedParameterResponse.status,
        error: new Error(`Could not load ${unprocessedParameterUrl}`),
      }
    }
    const rawUnprocessedParameter = yaml.load(
      await unprocessedParameterResponse.text(),
      {
        schema: yaml.JSON_SCHEMA, // Keep dates as strings. TODO: Change?
      },
    )
    const [unprocessedParameter, unprocessedParameterError] =
      auditRawParameterToEditable(strictAudit, rawUnprocessedParameter)
    if (unprocessedParameterError !== null) {
      return {
        status: 400,
        error: new Error(
          `Parameter "${processedParameter.name}" is not editable, because its raw unprocessed parameter YAML file is not valid`,
        ),
      }
    }

    return {
      props: {
        parameter: unprocessedParameter,
        processedParameter,
      },
    }
  }

  export function newParameterRepositoryRawUrl(
    { branch, group, project, rawUrlTemplate }: App.SessionOpenFiscaRepository,
    parameter: Parameter,
  ): string | undefined {
    // Can't use:
    // return eval("`" + rawUrlTemplate + "`")
    // because using direct eval with a bundler is not recommended and may cause problems
    // (more info: https://esbuild.github.io/link/direct-eval)
    return new Function(
      "branch",
      "group",
      "project",
      "path",
      "return `" + rawUrlTemplate + "`",
    )(branch, group, project, parameter.file_path)
  }
</script>

<script lang="ts">
  import { laxAudit } from "@auditors/core"
  import type { Parameter } from "@openfisca/json-model"
  import {
    auditEditableParameter,
    convertEditableParameterToRaw,
    yamlFromRawParameter,
  } from "@openfisca/json-model"

  import { goto } from "$app/navigation"
  import { session } from "$app/stores"
  import NodeEdit from "$lib/components/parameters/NodeEdit.svelte"
  import ScaleEdit from "$lib/components/parameters/ScaleEdit.svelte"
  import ValueEdit from "$lib/components/parameters/ValueEdit.svelte"
  import { newSelfTargetAProps } from "$lib/urls"

  export let processedParameter: Parameter
  export let parameter: Parameter

  let errors: { [key: string]: unknown } = {}
  let originalParameter = { ...parameter }
  let reviewed = false
  let showErrors = false
  let showYaml = false
  let validRunParameter: Parameter | undefined = undefined

  $: runRawParameter =
    validRunParameter === undefined
      ? undefined
      : convertEditableParameterToRaw(validRunParameter)

  $: runRawParameterYaml =
    runRawParameter === undefined
      ? undefined
      : yamlFromRawParameter(runRawParameter)

  $: if (reviewed) {
    parameter = {
      ...parameter,
      last_review: new Date().toISOString().split("T")[0],
    }
  } else {
    parameter = {
      ...parameter,
      last_review: originalParameter.last_review,
    }
  }

  $: ((parameter) => {
    const [validParameter, parameterError] = auditEditableParameter(
      laxAudit,
      parameter,
    )
    errors = (parameterError as { [key: string]: unknown } | null) ?? {}
    validRunParameter =
      parameterError === null ? (validParameter as Parameter) : undefined
  })(parameter)

  async function save() {
    const [validParameter, parameterError] = auditEditableParameter(
      laxAudit,
      parameter,
    )
    if (parameterError !== null) {
      showErrors = true
      return
    }
    showErrors = false
    const url = `/parameters/${processedParameter.name}.json`
    const res = await fetch(url, {
      body: JSON.stringify(validParameter),
      headers: { "Content-Type": "application/json; charset=utf-8" },
      method: "PUT",
    })
    if (res.ok) {
      errors = {}
      await goto(`/parameters/${processedParameter.name}`)
    } else {
      console.error(
        `Error ${res.status} while updating parameter "${
          processedParameter.name
        }" at ${url}\n\n${await res.text()}`,
      )
      errors = { header: "L'enregistrement du paramètre a échoué." }
      showErrors = true
    }
  }
</script>

<svelte:head>
  <title
    >Édition | {processedParameter.name} | Paramètres | {$session.title}</title
  >
</svelte:head>

{#if showErrors && Object.keys(errors).length > 0}
  <div
    class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4"
    role="alert"
  >
    <p class="font-bold">Error</p>
    {#if typeof errors === "string"}
      <p>{errors}</p>
    {:else if errors.header !== undefined}
      <p>{errors.header}</p>
    {:else}
      <dl>
        {#each Object.entries(errors) as [key, message]}
          <dt>{key}</dt>
          <dd>{message}</dd>
        {/each}
      </dl>
    {/if}
  </div>
  <pre>{JSON.stringify(errors, null, 2)}</pre>
{/if}

<main class="flex items-center justify-center fond">
  <div class="bg-white max-w-screen-md ">
    <form class="flex-col items-start pb-4" on:submit|preventDefault={save}>
      <div class="p-10">
        <!-- Partie modification de l'intitulé-->
        <h1 class="font-serif text-gray-700 pt-7 pb-3">
          {#each [...iterParameterAncestors(processedParameter.parent)] as ancestor}
            <!-- svelte-ignore a11y-missing-attribute -->
            <a
              class="link text-gray-500"
              {...newSelfTargetAProps(`/parameters/${ancestor.name}`)}
              >{ancestor.title}</a
            >
            &gt;
          {/each}
          <p class="text-3xl font-bold mb-4 ">
            {processedParameter.title}
          </p>
        </h1>
        <h2 class="font-serif font-bold text-xl pt-7 pb-3">
          Modifier les intitulés
        </h2>
        <div class="flex-col">
          <div class="inline-flex">
            <label class="my-2 text-xs">
              Nom court
              <input
                class="flex font-serif rounded border-gray-400 focus:ring-2 "
                type="text"
                required={showErrors}
                bind:value={parameter.ux_name}
              />
              {#if showErrors && errors.ux_name !== undefined}
                <p>{errors.ux_name}</p>
              {/if}
            </label>
          </div>
          <div class="flex">
            <label class="text-xs my-2">
              Nom complet
              <textarea
                class="flex font-serif rounded border-gray-400 focus:ring-2 "
                cols="40"
                required={showErrors}
                rows="2"
                type="text"
                bind:value={parameter.description}
              />
              {#if showErrors && errors.description !== undefined}
                <p class="text-lg">{errors.description}</p>
              {/if}
            </label>
          </div>
          <div class="flex">
            <label class="text-xs my-2 ">
              Description
              <textarea
                class="flex font-serif italic rounded border-gray-400 focus:ring-2 "
                cols="40"
                rows="5"
                type="text"
                bind:value={parameter.documentation}
              />
              {#if showErrors && errors.documentation !== undefined}
                <p>{errors.documentation}</p>
              {/if}
            </label>
          </div>
        </div>
        <!-- Partie modification de la valeur-->

        <h2 class="font-serif font-bold text-xl pt-7 pb-3">
          Modifier les valeurs
          {#each [...iterParameterAncestors(parameter.parent)] as ancestor}
            {ancestor.title}
            &gt;
          {/each}
        </h2>

        {#if parameter.class === ParameterClass.Node}
          <NodeEdit {errors} bind:parameter {showErrors} />
        {:else if parameter.class === ParameterClass.Scale}
          <ScaleEdit {errors} bind:parameter bind:reviewed {showErrors} />
        {:else if parameter.class === ParameterClass.Value}
          <ValueEdit {errors} bind:parameter bind:reviewed {showErrors} />
        {/if}

        {#if parameter.referring_variables !== undefined}
          <section>
            <h1>Variables dépendantes</h1>
            <ul class="list-disc list-inside">
              {#each parameter.referring_variables as variableName}
                <li>
                  <!-- svelte-ignore a11y-missing-attribute -->
                  <a class="link" href="/variables/{variableName}"
                    >{variableName}</a
                  >
                </li>
              {/each}
            </ul>
          </section>
        {/if}
        <div class="inline-flex">
          <label class="mb-4 text-xs">
            Type de paramètre
            <select
              class="flex rounded border-1 mx-auto text-xs "
              required={showErrors}
              bind:value={parameter.class}
            >
              {#if parameter.class === undefined}
                <option disabled selected value={undefined}
                  >Sélectionnez…</option
                >
              {/if}
              {#each Object.values(ParameterClass) as parameterClass}
                <option value={parameterClass}
                  >{labelFromParameterClass(parameterClass)}</option
                >
              {/each}
            </select>
            {#if showErrors && errors.class !== undefined}
              <p>{errors.class}</p>
            {/if}
          </label>
        </div>
      </div>
      <div class="bg-gray-200 p-10 mt-10 rounded">
        <!-- Synthèse de la proposition-->
        <h1 class="font-serif text-3xl pb-3">Envoi de la proposition</h1>

        <!-- Partie boutons-->

        <div>
          <button
            class="underline text-black hover:text-le-bleu text-base"
            on:click={() => (showYaml = !showYaml)}
            type="button"
            >{#if showYaml}Masquer{:else}Voir le code qui sera envoyé au service
              LexImpact (format YAML){/if}</button
          >
          {#if showYaml}
            {#if Object.keys(errors).length > 0}
              <div
                class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4"
                role="alert"
              >
                <p class="font-bold">Error</p>
                {#if typeof errors === "string"}
                  <p>{errors}</p>
                {:else if errors.header !== undefined}
                  <p>{errors.header}</p>
                {:else}
                  <dl>
                    {#each Object.entries(errors) as [key, message]}
                      <dt>{key}</dt>
                      <dd>{message}</dd>
                    {/each}
                  </dl>
                {/if}
              </div>
              <pre>{JSON.stringify(errors, null, 2)}</pre>
            {/if}
            {#if runRawParameterYaml !== undefined}
              <pre
                class="bg-gray-100 p-5 text-xs whitespace-pre-wrap">{runRawParameterYaml}</pre>
            {/if}
          {/if}
        </div>

        <div class="my-4 flex justify-start">
          <button
            class="inline-flex items-center bg-le-bleu text-white shadow-md hover:bg-blue-900 px-5 rounded p-2 uppercase text-sm"
            disabled={!$session.hasGithubPersonalAccessToken}
            type="submit"
          >
            <!-- material icons - Send Black -->
            <svg
              class="flex mr-2 items-center fill-current"
              xmlns="http://www.w3.org/2000/svg"
              height="24px"
              viewBox="0 0 24 24"
              width="24px"
              fill="#000000"
              ><path d="M0 0h24v24H0z" fill="none" /><path
                d="M2.01 21L23 12 2.01 3 2 10l15 2-15 2z"
              /></svg
            >

            Envoyer
          </button>
        </div>
      </div>
    </form>
  </div>
</main>

<style>
  .fond {
    background-color: #ffffff;
    /* Polka dots - Heropatterns.com échelle réduite */
    background-image: url("data:image/svg+xml,%3Csvg width='8' height='8' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ded500' fill-opacity='0.4' fill-rule='evenodd'%3E%3Ccircle cx='3' cy='3' r='3'/%3E%3Ccircle cx='13' cy='13' r='3'/%3E%3C/g%3E%3C/svg%3E");
  }
</style>
