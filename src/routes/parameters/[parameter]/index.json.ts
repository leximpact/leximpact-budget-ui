import { strictAudit } from "@auditors/core"
import type { JsonValue, Parameter } from "@openfisca/json-model"
import {
  auditEditableParameter,
  convertEditableParameterToRaw,
  ParameterClass,
  yamlFromRawParameter,
} from "@openfisca/json-model"
import type { RequestHandler } from "@sveltejs/kit"
import { randomBytes } from "crypto"

import { metadata } from "$lib/metadata"
import { getParameter } from "$lib/parameters"
import config from "$lib/server/config"

const { githubPersonalAccessToken, openfiscaRepository } = config

export const put: RequestHandler = async ({ request, params }) => {
  if (githubPersonalAccessToken === undefined) {
    return {
      body: {
        errors: {
          header:
            "L'édition de paramètres est impossible car le serveur n'a pas de jeton d'accès à GitHub.",
        },
      },
      status: 403,
    }
  }

  const { parameter: name } = params
  const parameter = getParameter(name)
  if (parameter === undefined) {
    return { body: null, status: 404 }
  }
  const [validParameter, parameterError] = auditEditableParameter(
    strictAudit,
    await request.json(),
  )
  if (parameterError !== null) {
    return {
      body: { errors: parameterError as JsonValue },
      status: 400,
    }
  }

  // Note: validParameter has no file_path
  if (parameter.file_path === undefined) {
    return {
      body: {
        errors: {
          header: `L'édition du paramètre "${name}" est impossible car le paramètre est généré automatiquement et n'a pas de fichier source.`,
        },
      },
      status: 400,
    }
  }

  const rawParameter = convertEditableParameterToRaw(
    validParameter as Parameter,
  )

  // Get current SHA of parameter.
  const filePath =
    parameter.class === ParameterClass.Node
      ? parameter.file_path + "/index.yaml"
      : parameter.file_path
  const parameterContentUrl = `https://api.github.com/repos/${
    openfiscaRepository.group
  }/${
    openfiscaRepository.project
  }/contents/${filePath}?ref=${encodeURIComponent(openfiscaRepository.branch)}`
  const parameterContentResponse = await fetch(parameterContentUrl, {
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
  })
  if (!parameterContentResponse.ok) {
    return {
      body: await parameterContentResponse.text(),
      status: parameterContentResponse.status,
    }
  }
  const parameterContent = await parameterContentResponse.json()

  // Get SHA of latest commit of OpenFisca branch.
  const sourceBranchUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/git/refs/heads/${openfiscaRepository.branch}`
  const sourceBranchResponse = await fetch(sourceBranchUrl, {
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
  })
  if (!sourceBranchResponse.ok) {
    return {
      body: await sourceBranchResponse.text(),
      status: sourceBranchResponse.status,
    }
  }
  const sourceBranch = await sourceBranchResponse.json()

  // Create new branch.
  const newBranchName = `parameter_${name}_${randomBytes(4).toString("hex")}`
  const newBranchUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/git/refs`
  const newBranchResponse = await fetch(newBranchUrl, {
    body: JSON.stringify(
      { ref: `refs/heads/${newBranchName}`, sha: sourceBranch.object.sha },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "POST",
  })
  if (!newBranchResponse.ok) {
    return {
      body: await newBranchResponse.text(),
      status: newBranchResponse.status,
    }
  }

  // Update parameter & commit it.
  const parameterUpdateUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/contents/${filePath}`
  const parameterUpdateResponse = await fetch(parameterUpdateUrl, {
    body: JSON.stringify(
      {
        branch: newBranchName,
        commiter: {
          author: {
            email: "test@example.com",
            name: "Jean Dupont",
          },
        },
        content: Buffer.from(yamlFromRawParameter(rawParameter)).toString(
          "base64",
        ),
        message: `Édition du paramètre ${name}`,
        sha: parameterContent.sha,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "PUT",
  })
  if (!parameterUpdateResponse.ok) {
    return {
      body: await parameterUpdateResponse.text(),
      status: parameterUpdateResponse.status,
    }
  }
  /* const parameterUpdate = */ await parameterUpdateResponse.json()

  // Create pull request.
  const pullRequestUrl = `https://api.github.com/repos/${openfiscaRepository.group}/${openfiscaRepository.project}/pulls`
  const pullRequestResponse = await fetch(pullRequestUrl, {
    body: JSON.stringify(
      {
        base: openfiscaRepository.branch,
        // body:
        draft: false,
        head: newBranchName,
        // issue:
        maintainer_can_modify: true,
        title: `Édition du paramètre ${name}`,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Token ${githubPersonalAccessToken}`,
    },
    method: "POST",
  })
  if (!pullRequestResponse.ok) {
    return {
      body: await pullRequestResponse.text(),
      status: pullRequestResponse.status,
    }
  }
  /* const pullRequest = */ await pullRequestResponse.json()

  return { status: 204 }
}
