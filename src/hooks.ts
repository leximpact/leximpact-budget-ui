import type { GetSession } from "@sveltejs/kit"
import cookie from "cookie"

import config from "$lib/server/config"
import { oauth2Authenticator } from "$lib/server/oauth2"

const { githubPersonalAccessToken, openfiscaRepository } = config

export const getSession: GetSession = async (event) => {
  const { user } =
    oauth2Authenticator === undefined
      ? { user: undefined }
      : await oauth2Authenticator.getSession(event)
  const { headers } = event.request
  const cookies = headers.get("cookie")
  const jwt = cookies ? cookie.parse(cookies)?.svelteauthjwt : undefined
  return {
    authenticationEnabled: oauth2Authenticator !== undefined,
    baseUrl: config.baseUrl,
    hasGithubPersonalAccessToken: githubPersonalAccessToken !== undefined,
    jwt: jwt,
    matomo: config.matomo,
    openfiscaRepository: {
      branch: openfiscaRepository.branch,
      group: openfiscaRepository.group,
      project: openfiscaRepository.project,
      rawUrlTemplate: openfiscaRepository.rawUrlTemplate,
      urlTemplate: openfiscaRepository.urlTemplate,
    },
    portalUrl: config.portalUrl,
    title: config.title,
    user,
    budget: config.budget,
  }
}
