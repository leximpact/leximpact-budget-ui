# CHANGELOG

### 1.0.1 [!6](https://git.leximpact.dev/leximpact/leximpact-budget-ui/-/merge_requests/6)

# 1.0.0

[!5](https://git.leximpact.dev/leximpact/leximpact-budget-ui/-/merge_requests/5)
[!4](https://git.leximpact.dev/leximpact/leximpact-budget-ui/-/merge_requests/4)
[!3](https://git.leximpact.dev/leximpact/leximpact-budget-ui/-/merge_requests/3)
[!1](https://git.leximpact.dev/leximpact/leximpact-budget-ui/-/merge_requests/1)
(...)
[leximpact-socio-fiscal-ui !50](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-ui/-/merge_requests/50)
