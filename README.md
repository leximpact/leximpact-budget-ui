# Interface LexImpact budget

Ceci est le code source de l'[interface Web](https://budget.leximpact.an.fr/budget) de l'application LexImpact dédiée au calcul budgétaire de différents dispositifs (CSG, CRDS).
Il comprend une représentation graphique des effets de la loi sur la population et le budget.

## Présentation

Ce projet est construit sur la base du framework JavaScript [SvelteKit](https://kit.svelte.dev/), en [TypeScript](https://www.typescriptlang.org) et avec [TailwindCSS](https://tailwindcss.com/).

Il emploie l'API Web définie par [leximpact-budget-api](https://git.leximpact.dev/leximpact/leximpact-budget-api) pour faire appel au moteur de microsimulation socio-fiscale [OpenFisca-France](https://github.com/openfisca/openfisca-france) et calculer les effets de la loi.

## Configurer

Dans le répertoire de l'application `leximpact-budget-ui/`, créer un fichier `.env` en se basant sur l'exemple fourni :

```shell
cp example.env .env
```

Et adapter son contenu aux besoins du développement, de l'intégration ou de la production selon les commentaires indiqués dans l'`example.env`.

## Installer

### NodeJS

Ce projet fonctionne avec [NodeJS](https://nodejs.org/fr/) `version 16` ou supérieure.
Si vous disposez déjà de `NodeJS`, la version peut être vérifiée avec la commande suivante :

```shell
node --version
```

Sinon, `NodeJS` peut-être téléchargé depuis son [site officiel](https://nodejs.org/fr/download/).  
Ou si l'on dispose de [Node Version Manager](https://github.com/nvm-sh/nvm) :

```shell
nvm install 16
```

### Dépendances

Afin d'installer les dépendances, exécuter la commande suivante :

```shell
npm install
```

> L'environnement peut également être réinitialisé avec la commande `npm run clean`.

## Exécuter l'application

Deux modes d'exécution cohabitent et dépendent du cas d'usage que l'on a :

- le mode développeur permet de voir le produit du code ainsi que l'effet direct des modifications qui y sont apportées,
- ou le mode production qui permet de générer un build pour une mise en ligne sur serveur de production (ou une évaluation en local).

### Mode développeur

Dans un terminal, exécuter la commande :

```shell
npm run dev
```

La commande conserve la main tout en restant à l'écoute des modifications de code.

L'application peut alors être consultée dans un navigateur à l'adresse indiquée (par défaut : `http://localhost:3000`), mais la première fois il peut être nécessaire de la recharger plusieurs fois, le temps que toutes les dépendances se compilent.

> L'application peut ensuite être arrêtée dans le terminal avec les touches `Ctrl`+`C`.

Bravo, vous êtes prêts à utiliser et contribuer à `leximpact-budget-ui` ! 🎉

### Mise en production

Dans un terminal, exécuter la commande :

```shell
npm run build
```

Le build produit peut alors être testé localement avec la commande suivante :

```shell
npm run preview
```

> L'application peut ensuite être arrêtée dans le terminal avec les touches `Ctrl`+`C`.

Bravo, vous êtes prêts à utiliser `leximpact-budget-ui` ! 🎉

## Tester

Les tests unitaires de l'application sont développés selon le framework [Jest](https://jestjs.io/fr/). L'application étant développée en TypeScript, ce choix est propagé aux tests mais ceci nécessite l'emploi du transformateur [ts-jest](https://www.npmjs.com/package/ts-jest).

En particulier, deux fichiers de configuration définissent le comportement des tests :

- `./jest.config.js` est dédié aux tests,
- `./tsconfig.json` est dédié au traitement du TypeScript.

Pour exécuter les tests, il est nécessaire d'avoir déjà installé l'application et d'exécuter la commande suivante :

```shell
npm run test
```

Tous les tests doivent s'achever sans erreur.

## En savoir plus sur la structure de l'application

Les sources de l'application sont regroupées comme suit dans le répertoire `./src/`:

- `src/routes/` : les pages de l'application
  - `src/routes/__layout.svelte` est le template partagé par toutes les pages et initialise le contexte commun (via [setContext](https://svelte.dev/docs#run-time-svelte-setcontext)),
  - `src/routes/index.svelte` est la page principale de l'application.
- `src/lib/` : les composants et utilitaires de l'application qu'ils interviennent du côté client ou serveur.
  - `src/lib/auditors/config.ts` : l'audit des éléments de configuration nécessaires à l'application (du `.env`)
  - `src/lib/components/` : les composants Svelte de l'application
  - `src/lib/server/*.ts` : les utilitaires qui s'exécutent côté serveur
  - `src/lib/*.ts` : les utilitaires qui s'exécutent côté client.
